# Réaliser un solveur de Sudoku

## Présentation du jeu
Un _Sudoku_ classique est une grille de 9 lignes et 9 colonnes, don composée de 9 blocs distincts de 3 lignes et 3 colonnes.

Remplir une grille de Sudoku consiste à utiliser tous les chiffres de 1 à 9 pour chacun des 9 blocs de sorte que chaque ligne et chaque colonne de la grille ne comporte aucun chiffre en double.

![grille sudoku](img/sudoku_exemple.svg)

## Affichage d'une grille

!!! todo "A faire"
    Ecrire la fonction `affichage_grille` qui permet l'affichage d'une grille de sudoku. 
    
    Voici l'affichage attendu pour la grille ci-dessus.

    ```
    +-----+-----+------+
    |    5|    3|8     |
    |2    |     |  6   |
    |  1  |     |     1|
    +-----+-----+------+
    |     |4    |      |
    |8    |     |1 9   |
    |     |9    |     8|
    +-----+-----+------+
    |    4|    8|     3|
    |     |7   6|  5   |
    |7 3  |1   4|     9|
    +-----+-----+------+
    ```
## Est-ce qu'une grille complète est correctement remplie ?

```python
    # Trois grilles correctement remplies
    A = [   [3,6,7,9,4,1,2,8,5],
            [1,5,2,6,8,3,4,9,7],
            [4,9,8,7,5,2,1,6,3],
            [7,4,6,1,9,5,8,3,2],
            [8,1,9,2,3,7,6,5,4],
            [2,3,5,8,6,4,7,1,9],
            [9,2,1,5,7,8,3,4,6],
            [5,8,4,3,2,6,9,7,1],
            [6,7,3,4,1,9,5,2,8]
        ]  

    B = [   [4,2,7,9,5,1,6,8,3],
            [6,3,9,2,8,4,7,5,1],
            [8,5,1,7,6,3,9,2,4],
            [5,1,4,8,9,6,3,7,2],
            [9,7,6,4,3,2,8,1,5],
            [2,8,3,1,7,5,4,9,6],
            [3,9,2,6,1,8,5,4,7],
            [7,4,5,3,2,9,1,6,8],
            [1,6,8,5,4,7,2,3,9]
        ]

    C = [   [9,8,5,1,3,2,4,7,6],
            [2,7,3,9,4,6,5,1,8],
            [4,1,6,5,7,8,9,2,3],
            [1,6,7,4,8,9,2,3,5],
            [8,5,2,6,1,3,7,9,4],
            [3,9,4,2,5,7,8,6,1],
            [7,4,1,3,9,5,6,8,2],
            [5,2,9,8,6,1,3,4,7],
            [6,3,8,7,2,4,1,5,9]
        ]

    # une grille avec erreur:
    D = [   [9,8,5,1,3,2,4,7,6],
            [2,7,3,9,4,6,5,1,8],
            [4,1,6,5,7,8,9,2,3],
            [1,6,7,4,8,9,2,3,5],
            [8,5,2,6,1,3,7,9,4],
            [3,9,4,2,5,7,8,6,1],
            [7,4,1,3,9,5,6,8,2],
            [5,2,9,8,6,1,3,4,7],
            [5,3,8,7,2,4,1,5,9]
        ]
```



### Vérifications des lignes

!!! todo "A faire"

    * Ecrire une fonction vérifiant qu'**une** ligne contient une fois et une seule chacun des chiffres de 1 à 9.
    * Écrire une fonction vérifiant que **chaque** ligne contient une fois et une seule chacun des chiffres de 1 à 9.

```python
    def une_ligne_est_correcte(numero_ligne, grille):
        ''' Entrée :
                numero_ligne (int) : entier compris entre 0 et 8 donnant le numéro de la ligne de la grille à étudier
                grille (list[list[int]]): grille de sudoku 9 x 9
            Sortie :
                True si la ligne de grille contient une fois et une seule chacun des chiffres de 1 à 9.
                False sinon
        '''
        # A compléter

    def les_lignes_sont_correctes(grille):
        ''' Entrée :
                grille (list[list[int]]): grille de sudoku 9 x 9
            Sortie :
                True si toutes les lignes de la grille contiennent une fois et une seule chacun des chiffres de 1 à 9.
                False sinon
        '''
        # A compléter

```

### Vérifications des colonnes

!!! todo "A faire"

    * Ecrire une fonction vérifiant qu'**une** colonne contient une fois et une seule chacun des chiffres de 1 à 9.
    * Écrire une fonction vérifiant que **chaque** colonne contient une fois et une seule chacun des chiffres de 1 à 9.

```python
    def la_colonne_est_correcte(numero_colonne, grille):
        ''' Entrée :
                numero_colonne (int) : entier compris entre 0 et 8 donnant le numéro de la colonne de la grille à étudier
                grille (list[list[int]]): grille de sudoku 9 x 9
            Sortie :
                True si la colonne de grille contient une fois et une seule chacun des chiffres de 1 à 9.
                False sinon
        '''
        # A compléter

    def les_colonnes_sont_correctes(grille):
        ''' Entrée :
                grille (list[list[int]]): grille de sudoku 9 x 9
            Sortie :
                True si toutes les colonnes de la grille contiennent une fois et une seule chacun des chiffres de 1 à 9.
                False sinon
        '''
        # A compléter

```
### Vérifications des zones

#### Repérer les zones

Pour traiter le cas des zones carrées, nous allons faire un choix de numérotation des carrés et remarquer que nous pouvons décrire assez facilement les coordonnées de la cellule haut gauche de chaque carré à partir de cette numérotation.

Le choix de la numérotation des carrés est la suivante :

![zones du sudoku](img/sudoku_zones.svg)

!!! todo "A faire"

    * Vérifier que la cellule haut gauche de chaque carré a pour couple `(ligne, colonne)` le couple `(3 * (numero // 3), 3 * (numero % 3))` où `numero` est le numéro du carré.
    * En déduire un code de la fonction `contenu_zone`prenant en paramètre le numéro de la zone et la grille de sudoku et  permettant de créer la liste des chiffres contenus dans la zone  identifiée par son numéro.


#### Vérifier les zones


!!! todo "A faire"

    * Une fonction vérifiant qu'**un** des 9 carrés 3×3 contient une fois et une seule chacun des chiffres de 1 à 9.
    * Une fonction vérifiant que **chacun** des 9 carrés 3×3 contient une fois et une seule chacun des chiffres de 1 à 9.

??? "Aide : définition des fonctions"

    ``` python
        def la_zone_est_correcte(numero_zone, grille):
                ''' Entrée :
                        numero_zone (int) : entier compris entre 0 et 8 donnant le numéro de la zone de la grille à étudier
                        grille (list[list[int]]): grille de sudoku 9 x 9
                    Sortie :
                        True si la zone de grille contient une fois et une seule chacun des chiffres de 1 à 9.
                        False sinon
                '''
                # A compléter

        def les_zones_sont_correctes(grille):
            ''' Entrée :
                    grille (list[list[int]]): grille de sudoku 9 x 9
                Sortie :
                    True si toutes les colonnes de la grille contiennent une fois et une seule chacun des chiffres de 1 à 9.
                    False sinon
            '''
            # A compléter
    ```

### Vérification de la grille complète

!!! todo "A faire"

    Écrire  la fonction `la_grille_est_correcte` vérifiant qu'une grille est correctement remplie.

??? aide

    Il suffit de faire appel aux trois fonctions précédentes, c'est à dire de vérifier que chaque ligne, chaque colonne, chaque carré est correctement rempli.


### Factorisation du code

Dans le code complet de la question précédente, vous pouvez remarquer que les fonctions `les_zones_sont_correctes`, `les_colonnes_sont_correctes` et `les_lignes_sont_correctes` ont exactement la même structure.

Dans la fonction suivante ces trois fonctions sont remplacées par une seule fonction `les_blocs_sont_corrects`.

```python

 def les_blocs_sont_corrects(grille, fonction_bloc):
    """
        Entrée : 
            grille (list[list[int]]) : grille de sudoku 9 x 9
            fonction_bloc : fonction de vérification d'un bloc (ligne, colonne ou zone)
        Sortie :
            renvoie True si chaque bloc respecte les règles du Sudoku, False sinon.
    """
    for numero_bloc in range(9):
        if not fonction_bloc(numero_bloc, grille):
            return False
    return True 
```

!!! todo "A faire"
    Modifer votre fonction `grille_est_correcte` pour utiliser la fonction `les_blocs_sont_corrects`


## Résoudre un sudoku

Pour résoudre automatiquement un sudoku nous allons utilise le _backtracking_ (retour sur trace), méthode communément employée pour résoudre des problèmes en programmation.
