# Projet guidé : Création du jeu _Snake_


## Description du jeu

!!! quote ""   
    _Snake_ est un jeu dans lequel le joueur dirige un serpent qui doit slalomer entre les bords de l'écran et les obstacles qui parsème le niveau.
    Le joueur doit faire manger à son serpent des pommes allongeant à chaque fois la taille du serpent et augmente sa vitesse de déplacement. Alors que le serpent avance inexorablement, le joueur ne peut que lui indiquer une direction à suivre (en haut, en bas, à gauchen à droite) afin d'éviter que la tête du serpent ne touche les murs ou son propre corps. et constitue lui-même un obstacle.

<p style="text-align:right; font-style : italic"> d'après Wikipédia.fr </p>

<!-- !!! note
    === "Enoncé"
        Développer $3x(7 − 5x)$

    === "Solution détaillée"
        $3x(7 − 5x) = 3x \times 7 + 3x \times (-5x)$

        $3x(7 − 5x) = 21x-15x^2$

        La forme développée de $3x(7-5x)$ est $21x-15x^2$ -->


Pour la création du jeu nous allons utiliser le module Pyxel et la programmation orientée objet. 

Documentation officielle du module [Pyxel](https://github.com/kitao/pyxel/blob/main/doc/README.fr.md)

---

Après avoir importé le module Pyxel dans votre script Python, on crée une classe Jeu, pour regrouper l’ensemble des attributs et méthodes nécessaires.

Cette classe doit contenir les méthodes ```draw()``` et ```update()```.
La méthode ```draw()``` crée et positionne les objets, alors que la méthode ```update()``` met à jour les variables. 
A l’intérieur du constructeur de la classe, on spécifie d’abord la taille de la fenêtre avec la fonction ```init()``` :

```python
# taille de la fenetre 128x128 pixels
# ne pas modifier
pyxel.init(128, 128, title="Jeu Snake")
```


On lance l’application Pyxel avec la fonction run qui crée deux processus basés sur les méthodes ```draw()``` et ```update()``` :

```
pyxel.run(self.update, self.draw)
```
Cette instruction doit être la dernière du constructeur. La création d’une instance de la classe permet de lancer le jeu.


Ainsi voici  votre fichier _jeu.py_:

```python
import pyxel

class Jeu :
    def __init__(self):
        pyxel.init(192, 128, title = 'Snake')
        
        pyxel.run(self.update,self.draw)

    def update(self):
        """mise à jour des variables (30 fois par seconde)"""
        pass


    def draw(self):
        """création des objets (30 fois par seconde)"""
        pyxel.cls(0)
        self.snake.dessiner()

Jeu()

```


## Présentation des différentes étapes du projet.

* Etape 1 -- Création de serpent : son premier visuel et son déplacement.
* Etape 2 -- Création de la pomme : son visuel
* Etape 3 -- Création des murs : son visuel
* Etape 4 -- Ajouter les collisions entre les objets et : serpent/pomme, serpent/mur serpent/serpent et leurs conséquences :
    * serpent/pomme :
        - apparition d'une nouvelle pomme
        - accélération du serpent
        - agrandissement du serpent
    * serpent/mur et serpent/serpent :
        - arrêt du déplacement du serpent
        - fin de la partie.
* Etape 5 -- Création des images serpent, pomme, murs.
* Etape 6 -- Création des sons : musique générale, son quand le serpent mange, quand il perd ...




## Préparation de votre espace de travail

Il faut sauvegarder régulièrement les versions fonctionnelles. Ici nous allons faire des sauvegardes à la fin de chaque étape. Ainsi dans votre répertoire personnel :

1. Créer un dossier ```PR_Snake```
2. Créer un dossier ```PR_Snake/Etape1```
3. Créer un dossier ```PR_Snake/Etape2```
4. etc ...




## Etape 1 : Création du serpent
---

Nous allons représenter le serpent comme une succession de carrés de 8 pixels de côté, la tête sera de couleur jaune et le corps de couleur verte.

**TODO** Dessin

!!! question
    
    Quelle instruction permet de dessiner un carré aux coordonnées $(x;y)$ de 8 pixels de côté ?

### Les éléments du corps du serpent

| Classe SectionCorps |
|---|
|Attributs : |
|    * x (entier) : absicsse |
|    * y (entier) : ordonnée |
|    * largeur (entier) : nb de pixels en largeur |
|    * hauteur (entier) : nb de pixels en hauteur |
|    * est_tete (bool) : Vrai si c'est la tête du serpent |


Compléter la classe `SectionCorps` ci-dessous.


```python
class SectionCorps :
    def __init__(self, x, y, est_tete = False):
        # écrire ici
    
    def dessiner():
        # écrire ici
```

### Classe Serpent

La classe `Serpent` a pour premier attribut `corps`, une liste de `SectionCorps` et une méthode `dessiner` qui permet de dessiner le serpent.

A sa création, un serpent est composé d'une tête en $(x;y)$ et, temporairement, deux éléments sections situés en $(x-8;y)$ et $(x-16;y)$

??? aide

    ```python
    class Serpent :
        def __init__(self, ..., ...):
            self.corps = [ ..., ..., ...]
        
        def dessiner():
            ...
    ```

Il faut maintenant mettre à jour la classe principale `Jeu` :

1. On ajoute un attribut `snake` de la classe `Serpent` qui initialise la tête du serpent en $(32;32)$
2. Dans la méthode draw ajouter l’instruction `self.snake.dessiner()`


_A ce stade, lors de l’exécution du script, un carré immobile jaune suivi de deux carrés vert apparaissent au centre de la fenêtre._

### Déplacement du serpent

Nous allons faire avancer le serpent horizontalement, tel que lorsqu’il sort de la fenêtre il réapparaisse de l’autre côté.
On veut créer la méthode `Serpent:changement_direction()`(notation informelle pour indiquer que la méthode `changement_direction()` est une méthode de la classe `Serpent`.)

Chaque élément du corps du serpent prend la place de l’élément qui le précède. 

Recopier la méthode :
```python
def deplacement(self):
    # deplacement de la tete
    derniere_position_x = self.corps[0].x
    derniere_position_y = self.corps[0].y
    self.corps[0].x = (self.corps[0].x + self.corps[0].largeur) %pyxel.width

    # deplacement de la queue :
    for corps in self.corps :
        if corps == self.corps[0]:
            continue 
        else:
            position_actuelle_x = corps.x
            position_actuelle_y = corps.y
            
            # cette section de corps prend la place de la section devant elle
            corps.x = derniere_position_x
            corps.y = derniere_position_y

            derniere_position_x = position_actuelle_x
            derniere_position_y = position_actuelle_y
```

Il faut maintenant mettre à jour `Jeu:update()` en ajoutant `self.snake.dessiner()`.

#### Gestion de la vitesse de déplacement

A ce stade, _ce serpent se déplace très rapidement, trop rapidement._ 
Nous allons maîtriser sa vitesse : nous allons ralentir la vitesse de déplacement du serpent.

Pour ralentir sa vitesse nous allons déplacer le serpent après un délai. Il faut avoir des informations de temps : `import time`

!!! note "Classe Serpent : Ajout d'attribut"

    * `vitesse : float` initialisé à 1,5.

!!! note "Classe Jeu : Ajout d'attribut"

    * `temps_dernier_mouvement : time` contient l'heure à laquelle le dernier mouvement a été réalisé.

Compléter la méthode `Jeu:update()` pour que le serpent ne se déplace que si le temps écoulé depuis le dernier mouvement est supérieur à l'inverse de la vitesse de déplacement du serpent.

```python
def update():
    temps_de_la_maj = time.time()
    if temps_de_la_maj - ... >= ... :
        self.temps_dernier_mouvement = ...
        ...

```

??? abstract "Solution"

    ```python
    def update():
        temps_de_la_maj = time.time()
        if temps_de_la_maj - self.temps_dernier_mouvement >= 1/self.snake.vitesse :
            self.temps_dernier_mouvement = temps_de_la_maj
            self.snake.deplacement()
        self.snake.changement_direction()

    ```

#### Changement de direction du serpent

Le serpent peut avoir quatre directions différentes de déplacement : vers la droite, vers la gauche, vers le haut ou vers le bas. 
Pour décrire ces directions nous allons créer la classe statique (on ne crée pas d'instance de cette classe) `Direction`suivante :

```python 
class Direction :
    DROITE = 0
    BAS = 1
    GAUCHE = 2
    HAUT = 3
```

!!! note "Classe Serpent : Ajout d'attribut"

    * `direction : int` initialisé à Direction.DROITE

    Cet attribut indique la direction de déplacement du serpent, les valeurs qu'il peut prendre sont Direction.DROITE, Direction.GAUCHE, Direction.BAS ou Direction.HAUT.

Compléter la méthode `Serpent:changement_direction()`ci-dessous, permettant de changer la direction et modifier dans la méthode de déplacement `Serpent:deplacement` le déplacement de la tête pour prendre en compte le changement de direction. Le corps par construction de la méthode suivra alors le mouvement.

```python
def changement_direction(self):
        if pyxel.btn(pyxel.KEY_RIGHT) and self.direction != ... :
            self.direction = Direction. ...
        if pyxel.btn(...) and ... :
            self.direction = ...
        if ... :
            ...
        if ...:
            ...
```

??? abstract "Solutions"
    === "`changement_direction`"

        ```python
        def changement_direction(self):
            if pyxel.btn(pyxel.KEY_RIGHT) and self.direction != Direction.GAUCHE:
                self.direction = Direction.DROITE
            if pyxel.btn(pyxel.KEY_LEFT) and self.direction != Direction.DROITE:
                self.direction = Direction.GAUCHE
            if pyxel.btn(pyxel.KEY_UP) and self.direction != Direction.BAS :
                self.direction = Direction.HAUT
            if pyxel.btn(pyxel.KEY_DOWN) and self.direction != Direction.HAUT:
                self.direction = Direction.BAS
        ```

    === "`deplacement()`"

        ```python
        def deplacement(self):

        # deplacement de la tete
        derniere_position_x = self.corps[0].x
        derniere_position_y = self.corps[0].y

        # - gestion de la direction
        if self.direction == Direction.DROITE:
            self.corps[0].x = (self.corps[0].x + self.corps[0].largeur) % pyxel.width
        if self.direction == Direction.GAUCHE:
            self.corps[0].x = (self.corps[0].x - self.corps[0].largeur) % pyxel.width
        if self.direction == Direction.HAUT:
            self.corps[0].y = (self.corps[0].y - self.corps[0].hauteur) % pyxel.height
        if self.direction == Direction.BAS:
            self.corps[0].y = (self.corps[0].y + self.corps[0].hauteur) % pyxel.height

        # deplacement de la queue : à partir d'ici rien n'a changer
        ```


A ce stade à l'exécution du script, il ne se passe rien, il faut ajouter dans la méthode `Jeu:update()` l'appel à la méthode `Serpent:changement_direction()`

!!! bug "Remarque" 

    Cette gestion des directions n'est pas tout à fait satisfaisante, en effet si les changements demandés sont plus rapides que la mise à jour du serpent, des effets non désirés vont apparaître. 

    _On pourra discuter des différentes stratégies à mettre en oeuvre ensemble, une fois le projet bien avancé._

## Etape 2 : Création de la pomme
---

Si vous êtes bloqués voici les différentes classes créées jusqu'à présent.

??? abstract "Solutions"     
    === "Classe `Direction`"

        ```python
        class Direction :
            DROITE = 0
            BAS = 1
            GAUCHE = 2
            HAUT = 3
        ```
    
        
    === "Classe `Jeu`"

        ```python
        class Jeu :
            def __init__(self):
                pyxel.init(192, 128, title='Snake')
                self.snake = Serpent(32, 32)

                self.temps_dernier_mouvement = time.time()

                pyxel.run(self.update, self.draw)

            def update(self):
                """mise à jour des variables (30 fois par seconde)"""
                temps_de_la_maj = time.time()
                if temps_de_la_maj - self.temps_dernier_mouvement >= 1/self.snake.vitesse :
                    self.temps_dernier_mouvement = temps_de_la_maj
                    self.snake.deplacement()
                self.snake.changement_direction()

            def draw(self):
                """création des objets (30 fois par seconde)"""
                pyxel.cls(0)
                self.snake.dessiner()
        ```
        
    === "Classe `SectionCorps`"

        ```python
        class SectionCorps :       
            def __init__(self, x, y, est_tete=False):
                self.x = x
                self.y = y
                self.hauteur = 8
                self.largeur = 8
                self.est_tete = est_tete

            def dessiner(self):
                if self.est_tete:
                    pyxel.rect(self.x, self.y, self.largeur, self.hauteur, 10)
                else:
                    pyxel.rect(self.x, self.y, self.largeur, self.hauteur, 3)
        ```
        
    === "Classe `Serpent`"

        ```python
        class Serpent :
            def __init__(self, x, y):
                    self.corps = []
                    self.corps.append(SectionCorps(x, y, True))
                    self.corps.append(SectionCorps(x - 8, y))
                    self.corps.append(SectionCorps(x - 16, y))

                    self.direction = Direction.DROITE

                    self.vitesse = 1.5

                def dessiner(self):
                    for c in self.corps:
                        c.dessiner()

                def changement_direction(self):
                    if pyxel.btn(pyxel.KEY_RIGHT) and self.direction != Direction.GAUCHE:
                        self.direction = Direction.DROITE
                    if pyxel.btn(pyxel.KEY_LEFT) and self.direction != Direction.DROITE:
                        self.direction = Direction.GAUCHE
                    if pyxel.btn(pyxel.KEY_UP) and self.direction != Direction.BAS :
                        self.direction = Direction.HAUT
                    if pyxel.btn(pyxel.KEY_DOWN) and self.direction != Direction.HAUT:
                        self.direction = Direction.BAS

                def deplacement(self):

                    # deplacement de la tete
                    derniere_position_x = self.corps[0].x
                    derniere_position_y = self.corps[0].y

                    if self.direction == Direction.DROITE:
                        self.corps[0].x = (self.corps[0].x + self.corps[0].largeur) % pyxel.width
                    if self.direction == Direction.GAUCHE:
                        self.corps[0].x = (self.corps[0].x - self.corps[0].largeur) % pyxel.width
                    if self.direction == Direction.HAUT:
                        self.corps[0].y = (self.corps[0].y - self.corps[0].hauteur) % pyxel.height
                    if self.direction == Direction.BAS:
                        self.corps[0].y = (self.corps[0].y + self.corps[0].hauteur) % pyxel.height

                    # deplacement de la queue :
                    for corps in self.corps :
                        if corps == self.corps[0]:
                            continue
                        else:
                            position_actuelle_x = corps.x
                            position_actuelle_y = corps.y

                            # cette section de corps prend la place de la section devant elle
                            corps.x = derniere_position_x
                            corps.y = derniere_position_y

                            derniere_position_x = position_actuelle_x
                            derniere_position_y = position_actuelle_y        
        ```

Le serpent va manger des pommes qui s'affiche aléatoirement dans la fenêtre de jeu une à une.

Nous allons représenter la pomme comme un carré rouge de 8 pixels de côté.

| Classe `Pomme` |
|---|
|**Attributs** : |
|    x (entier) : absicsse |
|    y (entier) : ordonnée |
|    largeur (entier) : nb de pixels en largeur |
|    hauteur (entier) : nb de pixels en hauteur |
|    est_tete (bool) : Vrai si c'est la tête du serpent |
| **Méthodes** : |
|    * dessiner() |

!!! aide "Classe `Pomme`"

    Créer la classe `Pomme` correspondante.

!!! aide "Classe `Jeu`"

    * Ajouter un attribut `pomme` à la classe, la première pomme apparaît en $(60;32)$.
    * Modifier en conséquence les méthodes `update()`et `draw()` pour afficher la première pomme.


## Etape 3 : Création des murs
---

Si vous êtes bloqués voici les différentes classes créées jusqu'à présent.

??? abstract "Solutions"     
    === "Classe `Direction`"

        ```python
        class Direction :
            DROITE = 0
            BAS = 1
            GAUCHE = 2
            HAUT = 3
        ```
    
        
    === "Classe `Jeu`"

        ```python
        class Jeu :
            def __init__(self):
                pyxel.init(192, 128, title='Snake')
                self.snake = Serpent(32, 32)

                self.temps_dernier_mouvement = time.time()

                pyxel.run(self.update, self.draw)

            def update(self):
                """mise à jour des variables (30 fois par seconde)"""
                temps_de_la_maj = time.time()
                if temps_de_la_maj - self.temps_dernier_mouvement >= 1/self.snake.vitesse :
                    self.temps_dernier_mouvement = temps_de_la_maj
                    self.snake.deplacement()
                self.snake.changement_direction()

            def draw(self):
                """création des objets (30 fois par seconde)"""
                pyxel.cls(0)
                self.snake.dessiner()
        ```
        
    === "Classe `SectionCorps`"

        ```python
        class SectionCorps :       
            def __init__(self, x, y, est_tete=False):
                self.x = x
                self.y = y
                self.hauteur = 8
                self.largeur = 8
                self.est_tete = est_tete

            def dessiner(self):
                if self.est_tete:
                    pyxel.rect(self.x, self.y, self.largeur, self.hauteur, 10)
                else:
                    pyxel.rect(self.x, self.y, self.largeur, self.hauteur, 3)
        ```
        
    === "Classe `Serpent`"

        ```python
        class Serpent :
            def __init__(self, x, y):
                    self.corps = []
                    self.corps.append(SectionCorps(x, y, True))
                    self.corps.append(SectionCorps(x - 8, y))
                    self.corps.append(SectionCorps(x - 16, y))

                    self.direction = Direction.DROITE

                    self.vitesse = 1.5

                def dessiner(self):
                    for c in self.corps:
                        c.dessiner()

                def changement_direction(self):
                    if pyxel.btn(pyxel.KEY_RIGHT) and self.direction != Direction.GAUCHE:
                        self.direction = Direction.DROITE
                    if pyxel.btn(pyxel.KEY_LEFT) and self.direction != Direction.DROITE:
                        self.direction = Direction.GAUCHE
                    if pyxel.btn(pyxel.KEY_UP) and self.direction != Direction.BAS :
                        self.direction = Direction.HAUT
                    if pyxel.btn(pyxel.KEY_DOWN) and self.direction != Direction.HAUT:
                        self.direction = Direction.BAS

                def deplacement(self):

                    # deplacement de la tete
                    derniere_position_x = self.corps[0].x
                    derniere_position_y = self.corps[0].y

                    if self.direction == Direction.DROITE:
                        self.corps[0].x = (self.corps[0].x + self.corps[0].largeur) % pyxel.width
                    if self.direction == Direction.GAUCHE:
                        self.corps[0].x = (self.corps[0].x - self.corps[0].largeur) % pyxel.width
                    if self.direction == Direction.HAUT:
                        self.corps[0].y = (self.corps[0].y - self.corps[0].hauteur) % pyxel.height
                    if self.direction == Direction.BAS:
                        self.corps[0].y = (self.corps[0].y + self.corps[0].hauteur) % pyxel.height

                    # deplacement de la queue :
                    for corps in self.corps :
                        if corps == self.corps[0]:
                            continue
                        else:
                            position_actuelle_x = corps.x
                            position_actuelle_y = corps.y

                            # cette section de corps prend la place de la section devant elle
                            corps.x = derniere_position_x
                            corps.y = derniere_position_y

                            derniere_position_x = position_actuelle_x
                            derniere_position_y = position_actuelle_y     
        ```

    === "Classe `Pomme`"

        ```python
        class Pomme :
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.hauteur = 8
                self.largeur = 8

            def dessiner(self):
                pyxel.rect(self.x,self.y, self.largeur, self.hauteur, 8)
        ```

Pour cette version du jeu, la fenêtre est entièrement encadrée par des murs. Nous allons représenter un mur par un carré orange de 8 pixels de côté.

!!! aide "Classe `Mur`"

    Créer la classe `Mur` sur le modèle de la classe `Pomme`

!!! aide "Classe `Jeu`"

    * Ajouter un attribut `murs`, une liste de `Mur`, l'initialisé pour faire le contour de la fenêtre de jeu.
    * Modifier en conséquence la méthode `draw()` pour afficher la muraille.



## Etape 4 : Ajouter les collisions
---

A présent on veut la suppression de la pomme lorsque le serpent la mange avec l’apparition d’une nouvelle pomme et l'allongement de son corps, et la fin de la partie si le serpent entre en collision avec le mur ou lui-même.

### Recherche des collisions

#### Classe `Pomme`

!!! question

    A quelles conditions le serpent entre en collision avec une pomme ?

??? tip "Aide"
    
    === "Que signifie 'entrer en collision' ?"

        Il y a collision entre deux éléments lorsque leurs représentations se chevauchent.


Lorsqu'une pomme est mangée, plutôt que de la supprimer et d'en créer une nouvelle aléatoirement dans la fenêtre de jeu, nous allons simplement déplacer la pomme existante !

!!! note "A faire"

    1. La méthode `Pomme:en_contact()`qui prend entre autres 4 entiers en paramètre `x,y,l,h`et qui renvoie un booléen si la pomme est en contact avec le rectangle aux coordonnées $(x;y)$, de largeur `l` et de hauteur `h`.
    2. La méthode `Pomme:deplacement()` qui déplace la pomme aux nouvelles coordonnées (new_x;new_y)

Il nous faut maintenant une nouvelle méthode dans la classe `Jeu` qui va déterminer une position acceptable pour la nouvelle pomme.

!!! note "A faire"

    Ecrire cette méthode `Jeu:deplacement_pomme` qui détermine aléatoirement les nouvelles coordonnées de la pomme pour qu'elle ne soit pas en collision avec un mur ni le serpent.


    !!! example "Intéressez vous à la fonction `randrange()` du module `random`."



<!-- !!! example "Point Python"

        Intéressez vous à la fonction `randrange()` du module `random`.  -->

??? tip "Aide "

    === Aide 1

        Quelle partie du serpent peut entrer en collision avec la pomme ?

    === Aide : Squelette de la méthode

    ```python
    def deplacement_pomme(self):
            # déterminer aléatoire des coordonnées aléatoire de la pomme
            # S'assurer quelle n'est pas sur un mur ou dans le corps du serpent
            position_valable = False
            while not position_valable :
                new_x = random.randrange(8,184,8) #nombre entre 8 et 184 et doit etre un multiple de 8
                new_y = random.randrange(8,120,8) #nombre entre 8 et 184 et doit etre un multiple de 8
                position_valable = True
                # Ne pas etre sur le corps du serpent
                for s in self.snake :
                    if (... 
                        and ... 
                        and ...
                        and ... ):
                        position_valable = False
                        break
                # ne pas etre sur un mur : actuellement c'est déjà géré

            # Lorsque les coordonnées sont valides, on déplace effectivement la pomme
            self.pomme.deplacement(..., ...)
    ```    
#### Classe `Mur`

!!! note "Classe `Mur`"

    Ecrire la méthode `en_contact()` similaire à celle de la classe `Pomme`.

#### Classes `SectionCorps` et `Serpent`

!!! note "Classe `SectionCorps`"

    Ecrire la méthode `en_contact()` similaire aux précédentes

!!! note "Classe `Serpent`"

    Ecrire la méthode `se_mange_la_queue()` qui renvoie le booléen `True` si le serpent entre en collision avec une partie de son corps.




### Prise en compte des collisions

Cette prise en compte se fait dans la classe principale du jeu `Jeu`. Il nous faut donc une nouvelle méthode `verification_collisions()`. 

!!! note "A faire"

    Ecrire cette fonction qui, pour le moment ne fait rien !

Si le serpent entre en collision avec un mur ou lui-même, le serpent meure et la partie est terminée, tout mouvement s'arrête et on affiche GAME OVER, en blanc, au milieu de la fenêtre.


#### Collisions fatales : se prendre un mur ou se manger la queue

!!! note "Classe Serpent : Ajout d'attribut"

    * `est_vivant : bool` initialisé à `True`

Ainsi lorsque que le serpent entre en collision avec un mur ou lui-même, cet attribut devient `False`.

!!! note " A faire"

    1. Compléter la méthode `Jeu:verification_collisions()`
    1. Modifier la méthode `Jeu:draw()`pour qu'elle affiche GAME OVER aux coordonnées $(50;64)$.
    2. Modifier la méthode `Jeu.update()`pour que le serpent n'avance plus s'il est mort.

??? tips "Aide"

    à voir

#### Collisions heureuses : Manger une pomme

Lorsque le serpent mange une pomme nous devons gérer plusieurs choses :

* la pomme doit être déplacée à une nouvelle position
* la vitesse du serpent est augmentée de 10 %
*  le serpent doit grandir


!!! note " A faire"

    Mettre à jour la méthode `Jeu:verification_collisions()` pour gérer les deux premiers points.



##### Croissance du serpent

A chaque pomme mangée, le serpent grandit d'une section de corps supplémentaire.

!!! note "Classe `Serpent`" 

    1. Créer une méthode `croissance`qui agrandit le serpent d'une section aux coordonnées de la dernière section.
    2. Quelle méthode doit-on alors mettre à jour, pour prendre en compte ces ajouts ?


**TODO** Insérer image

A ce stade, le jeu est simple mais fonctionnel. Reste maintenant à travailler " l'esthétisme" des éléments (serpents, pomme, murs)

## Etape 5 : Amélioration du visuel
---
Pour ouvrir l’éditeur d’image : ouvrir la console (avec Edupyter depuis Thonny : "Outils" > "Ouvrir la console du système...", ou depuis le menu d’Edupyter : "Console") et saisir : 

`pyxel edit snake.pyxres`

Si le fichier de ressource Pyxel (.pyxres) existe déjà, le fichier est chargé, sinon, un nouveau fichier avec le nom indiqué est créé.

### Création des images 

Reproduire les images de la tête du serpent, son corps, la pomme et le mur comme ci-dessus.

Utiliser la documentation Pyxel pour avoir de l'aide dans la section [Comment créer une ressource](https://github.com/kitao/pyxel/blob/main/doc/README.fr.md#comment-créer-une-ressource)

!!! note "A faire"

    Ajouter dans l'initialisateur de la classe `Jeu` les lignes suivantes :

    ```python
    
    # chargement des images
    pyxel.load("snake.pyxres")
    ```

A l'aide de la documentation sur la fonction `pyxel.blt` modifier les méthodes `dessiner()`dans les classes `Pomme`, `Mur`et `SectionCorps` pour  remplacer les carrés colorés par leurs images nouvellement créées.



## Etape 6 : Ajout du son
---

Création du son dans le fichier `snake.pyxres`, voir la documentation pour plus de renseignements.


## Etape 7 : Faire évoluer le jeu de base
---

Voici des exemples pistes d'évolution :

* Compter le nombre de pommes mangées et/ou l'associer à un score.
* Ajouter des niveaux, des difficultés :
    * Les murs peuvent être placés ailleurs, 
    * La vitesse de déplacement peut être modifiée, 
    * Les pommes peuvent ne plus être mangeables, disparaitre après un certain temps avant de réapparaître plus loin ...
    * Nouveau niveau = mise à zéro la taille du serpent
* A vos idées ! 



<p style="text-align:right;"> D'après une idée de CaffeinatedTech, vidéo Youtube Python Retro Game Tutorial. </p>