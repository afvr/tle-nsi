# Projet de rentrée : Survie des Bakos

!!! info "Notions réinvesties"

    - Création de fonctions, Répétition d'instructions, Instructions conditionnelles, Tableaux à deux dimensions
    - Parcours séquentiel de tableau

__La planète Okab explose, seuls quelques Bakos, habitants de la planète, arrivent à s'échapper. Il traverse l'espace et arrivent sur la planète Gacrux. Mais cette planète n'est pas si hospitalière qu'espérer ...__

Chaque Bako a atterri aléatoirement sur Gacrux.

!!! example "Règles de survie sur la planète"
    - Un Bako survit seulement s'il a 2 ou 3 voisins, sinon il meurt d'isolement ou de surpopulation.
    - Un Bako apparaît sur un espace libre s'il y a exactement 3 Bakos autour.

**Les Bakos vont-ils survivre ?**



## Simulation de la situation

On considère que la planète Gacrux est un carré de $n\times n$ cases où chaque case ne peut accueillir qu'un Bako. Chaque Bako a au maximum 8 voisins.


!!! abstract "Exemple pour une planète de taille $5\times5$"
    === "Jour 0 : Atterissage sur Gacrux"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J1.png)


    === "Jour 1"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J2.png)


    === "Jour 2"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J3.png)


    === "Jour 3"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J4.png)


    === "Jour 4 : Quelle est la situation ?"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J5.png)

## Travail à Faire

L'objectif du projet est de créer un ensemble de fonctions permettant de simuler la survie des Bakos atterris sur Gacrux.

Pour suivre l'évolution de la population de Bakos, il est demandé également de permettre l'affichage de l'état d'occupation de la planète après un certain nombre de jours.







!!! note "A faire 1 : Le Big Bang"
    Créer une fonction `creation_planete()` qui prend en argument un entier $n$ et renvoie un tableau de tableau de taille $n\times n$ dont chaque case est la chaîne `'.'`.

!!! note "A faire 2 : L'atterrissage"
    === " "
        Créer une fonction `atterrissage()` qui prend deux arguments : `nb_bakos` (`int`) et `planete` (`list(list(str))`) où `nb_bakos` est le nombre de bakos qui atterrissent sur la planete `planete`. Chaque Bakos en vie est représenté par la chaîne `'B'`.


    === "Aide"
        Il faut utiliser la fonction `randint` du module `random`.

        On ne peut ajouter un Bakos que sur une case "vide".


    === "Code à trous"


!!! note "A faire 3 : Vue du ciel"
    Créer une fonction `affichage()` qui prend en argument `planete` (`list(list(str))`) et qui affiche l'état d'occupation de la planète.

    !!! done "Exemple d'affichage"

        Voici l'affichage textuel attendu du jour 0 de l'exemple précédent :

          ```
          . B . B B

          . . . B .

          . . B B .

          . . B . .

          . B . . .
          ```
!!! note "A faire 4 : Les voisins"
    Le but est de créer une fonction `nb_voisins()` qui prend trois arguments `planete` (`list(list(str))`), `i` (`int`) et `j` (`int`) qui renvoie le nombre de Bakos voisins de la case `planete[i][j]`.

    On considère la case `planete[i][j]`

    1. Dans cette question la case est un coin de la planète
        1. Quels sont les couples d'indices possible pour cette case ?
        1. Combien a-t-elle de cases voisines ?
        3. Quels sont les indices de ces cases voisines ?
    2. Dans cette question la case est au bord de la planète (hors coin).
        1. Combien a-t-elle de cases voisines ?
        2. A quelles conditions sur `i` et `j` la case est-elle au bord ?
        3. Quels sont les indices de ses cases voisines ?

    3. Dans cette question on considère que c'est une case à l'intérieur de la planète.
        1. Combien a-t-elle de cases voisines ?
        3. Quels sont les indices de ses cases voisines ?

    4. En déduire la fonction `nb_voisins`.

!!! note "A faire 5 : Le jour d'après"
    === " "

        !!! quote "On rappelle les règles de survie des Bakos "
            - Un Bako survit seulement s'il a 2 ou 3 voisins, sinon il meurt d'isolement ou de surpopulation.
            - Un Bako apparaît sur un espace libre s'il y a exactement 3 Bakos autour.

        **Remarque**. Chaque Bako créée ou mort au cours de cette journée n'est pris en compte qu'une fois la journée terminée.

        Pour cette raison, il est nécessaire de procéder en deux étapes :

        1. Identifier les naissances et les décès par un code temporaire :
        Quand un Bako meurt on remplace la chaîne `'B'`par la chaîne `'m'`. Quand un Bako apparaît on remplace la chaîne `'.'` par la chaîne `'b'`.
        2. Puis supprimer les morts (remplacer la chaîne `'m'` par la chaîne `'.'`) et valider les naissances (remplacer la chaîne `'b'` par la chaîne `'B'`).

        Créer la fonction `nouveau_jour()` qui prend en argument `planete` (`list(list(str))`) et qui la met à jour après qu'une journée se soit passé et que les règles de survie soient appliquées


    === "Aide"
        Parcourir successivement deux fois la `planete`

        Penser à utiliser la fonction `nb_voisins`


!!! note "A faire 6 : Jour après jour"
    Créer le script principal, simulant l'évolution de la population de Bakos étant donné un nombre de jour. Afficher alors la situation.
