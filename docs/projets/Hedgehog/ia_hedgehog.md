# Projet : Le hérisson gourmand

## Présentation du jeu 

_Hedgehog_ est un jeu créé par Andra Sabbatini.

Un hérisson a élu domicile dans un jardin.
Ce jardin est découpé en 4 parcelles. Dans chaque parcelle n'est cultivée qu'une seule variété de légumes.

<p float="center">
  <img src="../img/grille.png" width="30% of window" />
  <img src="../img/rotation.png" width="30% of window" /> 
  <img src="../img/exemple_affectation.png" width="30% of window" />
</p>


Les hérissons sont omnivores, mais celui-ci a un schéma alimentaire très particulier.
Il mange quelque chose de différent chaque jour, se déplaçant dans le sens des aiguilles d'une montre.

Chaque jour il creuse en ligne droite et apparaît dans la parcelle suivante. Mais il n'apparaît jamais deux fois au même endroit (il n'y a plus rien à manger !)

Le but est donc de déterminer le jour où chaque légume a été mangé.  Dernière précision, une fois tous les légumes dévorés le hérisson doit pouvoir repartir d'où il a commencé ! (Dans notre exemple après le jour 36 il doit être capable d'aller à l'emplacement du jour 1.)

## Compréhension du jeu 

!!! exercice

  Voici une grille de jeu _jardin1_

  ![jardin1](img/jardin1.svg)

  1. Placer les numéros 3, 5, 8, 9, 13 et 19.
  2. Compléter la grille.


!!! exercice

  Voici deux autres grilles (_jardin109_ et _jardin110_)

  ![jardin109](img/jardin109.svg)
  ![jardin110](img/jardin110.svg)

  Les compléter, en respectant les règles du jeu.

  ## Description du projet

Dans ce projet vous allez créer un programme permettant à l'ordinateur de trouver la solution d'une grille de jeu.

Vous devez utiliser la programmation orientée objet pour ce projet.

### Modélisation du problème

#### La classe Jardin

Pour construire la classe `Jardin`

1. Déterminer ses attributs
2. Créer une méthode `importer_grille` permettant d'importer une grille de jeu à partir d'un fichier texte dont le format est le suivant : 
  * La première ligne contient un mot _jardin_ correspondant au type de grille, car il y a des variantes.
  * Chaque ligne suivante comporte le même nombre d'entiers positifs : 
    * 0 représente une case vide
    * les autres entiers représentent le numéro du jour où le hérisson est passé par la case.
3. Créer une méthode permettant d'afficher sur la console la grille de jeu dont voici le résultat pour la grille _jardin1_

  ```
  +---------------------+
  | 30 14 2  | 31 15 0  | 
  | 22 0  18 | 0  23 0  | 
  | 0  10 6  | 11 7  27 | 
  +----------+----------+
  | 29 0  17 | 32 16 28 | 
  | 0  0  1  | 36 0  0  | 
  | 0  0  0  | 12 24 4  | 
  +----------+----------+
  ```

  



### Résolution automatique d'une grille

<!-- La résolution de la grille de jeu se fait récursivement, la méthode consiste à tester tous les nombres et à revenir en arrière si la solution n'est pas satisfaisante on appelle ça le _backtracking_. -->
Vous allez utiliser le _Backtracking_ pour résoudre la grille (sur le même principe que pour le [_Sudoku_](../../TP/ia_sudoku.md)).
Trois cas s'envisagent :

* **Case de base** si toute la grille est remplie c'est qu'on est arrivé à remplir la grille donc on renvoie `True`
  * **Cas général 1** : si la prochaine case est déjà remplie on passe à la suivante.
  * **Cas général 2** : si la prochaine case est vide alors :
    * Pour chaque nombre possible
        * on place le nombre dans la case
        * on lance la résolution de la grille complétée		
            * si True alors renvoie `True`
            * sinon on remet à 0 la case
    * Aucune valeur n'a convenue : on renvoie `False`.
