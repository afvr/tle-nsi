import drawSvg as draw

TAILLE_CASE = 40
TAILLE_POLICE = 20

def importer_grille(nom_fic):
    grille = []
    with open(nom_fic, "r") as fichier:
        lignes = fichier.readlines()
    # la premiere ligne correspond au nom de la variante de jeu (potager par défaut)
    for i in range(1,len(lignes)) :
        boardLine = list(map(int, lignes[i].rstrip('\n').split(' ')))
        grille.append(boardLine)
    return grille

def grille_to_svg(grille, fichier):
    ''' créer l'image svg de la grille 'jardin' de jeu'''
    largeur, hauteur = TAILLE_CASE*(len(grille[0])), TAILLE_CASE*(len(grille))
    d = draw.Drawing(largeur + TAILLE_CASE, hauteur + TAILLE_CASE, origin=(-0.5*TAILLE_CASE,-0.5*TAILLE_CASE))

    r = draw.Rectangle(0, 0, largeur, hauteur, fill='white')
    d.append(r)

    n = len(grille)
    for y in range(n):
        for x in range(n):
            r = draw.Rectangle(x*TAILLE_CASE,
                               y*TAILLE_CASE,
                               TAILLE_CASE, TAILLE_CASE, fill="none",
                               stroke_width=1,
                               stroke='gray')
            d.append(r)
            if grille[n-y-1][x] > 0:
                d.append(draw.Text(str(grille[n-y-1][x]),
                           TAILLE_POLICE,
                           x*TAILLE_CASE + 0.5*TAILLE_CASE,
                           y*TAILLE_CASE + 0.5*TAILLE_CASE,
                           text_anchor='middle',
                           valign='middle'))
#str(n-y-1)+','+str(x),
    taille_zone = n//2
    print(taille_zone)
    for y in range(2):
        for x in range(2):
            r = draw.Rectangle(x * TAILLE_CASE * taille_zone,
                               y * TAILLE_CASE * taille_zone,
                               TAILLE_CASE * taille_zone, TAILLE_CASE * taille_zone, fill="none",
                               stroke_width=3,
                               stroke='black')
            d.append(r)


    d.savePng(f'{fichier}.png')
    d.saveSvg(f'{fichier}.svg')


grille = importer_grille("../grilles/jardin1.txt")
grille_to_svg(grille,"jardin1")
grille = importer_grille("../grilles/jardin109.txt")
grille_to_svg(grille,"jardin109")
grille = importer_grille("../grilles/jardin110.txt")
grille_to_svg(grille,"jardin110")
