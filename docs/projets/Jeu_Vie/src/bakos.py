def creation_planete(n):
    ''' crée la matrice n*n représentant la planète Gacrux'''
    return ['' for i in range(n) for j in range(n)]

def atterissage(nb_bakos,planete):
    ''' ajoute aléatoirement n Bakos sur la planète Gacrux
            planete[i][j] = 'B' quand un Bakos est présent sur la case'''
    nb_bakos_places = 0
    n = len(planete)
    while nb_bakos_places != nb_bakos :
        i = randint(n)
        j=randint(n)
        if planete[i][j] == '':
            planete[i][j] = 'B'
            nb_bakos_places +=1

def nb_voisins(planete,i,j):
    nb_voisins = 8
    n = len(planete)
    for x in (-1,0,1):
        for y in (-1,0,1):
            if 0<=i+x<=n-1 and  0<=j+y<=n-1 and planete[i+x][j+y] == 'B':
                nb_voisins += 1
    return nb_voisins
            
                
        

def nouveau_jour(planete):
    ''' applique les règles de survie sur la planète pour le jour suivant, on ne considère que les bakos présents ce jour-là'''
    ''' 1er temps : bako mort => m, bako né => b
        2nd temps : on supprime les morts et b-> B'''
    n = len(planete)
    for i in range(n):
        for j in range(n):
            n_voisins = nb_voisins(planete,i,j)
            if planete[i][j] == 'B' and (n_voisins < 2 or n_voisins > 3) :
                planete[i][j] == 'm'
            elif planete[i][j] == '' and n_voisins == 3:
                planete[i][j] == 'b'
    
       
    for i in range(n):
        for j in range(n):
            if planete[i][j] == 'b':
                planete[i][j] == 'B'
            elif planete[i][j] == 'm':
                planete[i][j] == ''


            
    
    
    
    
        