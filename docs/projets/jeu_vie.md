# Projet de rentrée : Survie des Bakos




!!! info "Notions réinvesties"

    - Création de fonctions, Répétition d'instructions, Instructions conditionnelles, Tableaux à deux dimensions
    - Parcours séquentiel de tableau

__La planète Okab explose, seuls quelques Bakos, habitants de la planète, arrivent à s'échapper. Il traverse l'espace et arrivent sur la planète Gacrux. Mais cette planète n'est pas si hospitalière qu'espérer ...__

Chaque Bako a atterri aléatoirement sur Gacrux.

!!! example "Règles de survie sur la planète"
    - Un Bako survit seulement s'il a 2 ou 3 voisins, sinon il meurt d'isolement ou de surpopulation.
    - Un Bako apparaît sur un espace libre s'il y a exactement 3 Bakos autour.

**Les Bakos vont-ils survivre ?**



## Simulation de la situation

On considère que la planète Gacrux est un carré de $n\times n$ cases où chaque case ne peut accueillir qu'un Bako. Chaque Bako a au maximum 8 voisins.


!!! abstract "Exemple pour une planète de taille $5\times5$"
    === "Jour 0 : Atterissage sur Gacrux"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J1.png)


    === "Jour 1"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J2.png)


    === "Jour 2"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J3.png)


    === "Jour 3"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J4.png)


    === "Jour 4 : Quelle est la situation ?"

        ![Ceci est un exemple d’image](Jeu_Vie/Bakos_J5.png)

## Travail à Faire

L'objectif du projet est de créer un ensemble de fonctions permettant de simuler la survie des Bakos atterris sur Gacrux.

Pour suivre l'évolution de la population de Bakos, il est demandé également de permettre l'affichage de l'état d'occupation de la planète après un certain nombre de jours.

Pour rappel, la taille de la planète peut variée (en respectant sa forme), le nombre de Bakos qui atterrissent peut varié et leur position de départ sur la planète est aléatoire.


!!! done "Exemple d'affichage"

    Voici l'affichage textuel du jour 0 de l'exemple précédent :

      ```
      . B . B B

      . . . B .

      . . B B .

      . . B . .

      . B . . .
      ```




??? note "Aide"
    Voici quelques étapes :

    * Création de la planète Gacrux
    * Affichage de la planète
    * Placement initial (aléatoire) des Bakos sur la planète
    * Appliquer les règles de survie

??? note "Je suis complètement perdu"
    Voir le projet détaillé [ici](jeu_vie_details.md)
