# Projet de rentrée : Gestion des populations de Hagues et de Tiakis

!!! info "Notions réinvesties"
    - Création de fonctions, Répétition d'instructions, Instructions conditionnelles, Tableaux à deux dimensions
    - Dictionnaires et tableau de dictionnaires
    - Parcours séquentiel de tableau


__Dans les mers d'Elgafar, deux types d'animaux marins se cotoîent : les hagues et le tiakis.
Les tiakis sont les proies préférées des hagues.__

!!! example "Règles d'évolution d'un Tiaki"
    - Chaque jour, il se déplace aléatoirement sur une case libre parmi ses voisines ou ne se déplace pas
    - Après $t$ jours de gestation, il donne naissance à un nouveau Tiaki sur une case voisine libre (aléatoire).

!!! example "Règles d'évolution d'un Hague"
    - Chaque jour, il perd un point d'énergie.
    - Chaque jour, il se déplace sur la case d'un tiaki présent parmi ses cases voisines et son énergie remonte à son maximum ou il se déplace sur une case libre voisine.
    - Lorsque son énergie est à 0, il meurt.
    - Après $h$ jours de gestation, il donne naissance à un nouveau hague sur une case voisine libre (aléatoire).

__ Comment évolue la population de chacune des deux espèces ?__


## Simulation de la situation

On considère que les mers d'Elgafar sont représentées par un carré de $n^2$ cases où chaque case ne peut accueillir qu'un seul animal marin.


!!! abstract "Exemple des mers d'Elgafar de taille $5\times 5$"
    Pour cet exemple on considère que 5 tiakis et 1 hague peuplent initialement les mers d'Elgafar.

    - Tiakis : leur temps de gestation est de 2 jours.
    - Hague : leur energie est de 5 jours et leur temps de gestation de 3.

    === "Jour 0 : Position initiale"
        ![J0 Position initiale](Jeu_Vie/Tiakis_Hague_J0.png)


    === "Jour 1"
        ![J1 Evolution](Jeu_Vie/Tiakis_Hague_J1.png)


    === "Jour 2"
        ![J2 Evolution](Jeu_Vie/Tiakis_Hague_J2.png)


    === "Jour 3"
        ![J3 Evolution](Jeu_Vie/Tiakis_Hague_J3.png)

## Travail à Faire

L'objectif du projet est de créer un ensemble de fonctions permettant de simuler l'évolution des populations de Tiakis et de Hagues au fil des jours.

Pour suivre leur évolution, il est demandé de permettre l'affichage de l'état d'occupation des mers d'Elgafar après un certain nombre de jours.

!!! done "Exemple d'affichage"
    Voici l'affichage textuel du jour 3 de l'exemple précédent (T pour un tiaki, Hn pour un hague de $n$ points d'énergie, . pour une case vide) :
      ```
      T  T  .  .  .
      .  .  .  T  .
      .  .  T  .  .
      T  .  H2 H5 .
      .  T  .  .  T
      ```
