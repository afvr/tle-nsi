def newcolor(name,hexcode):
    print(f'--color-{name} : #{hexcode};')
    return f'--color-{name} : #{hexcode};\n'




def boite(nom_affiche, nom_boite, icone, color):


    icn = '--md-admonition-icon--' + nom_boite
    col = '--color-' + nom_boite
    code = f'/* début {nom_affiche} */\n\n'

    if icone is not None :
        code  += f"--md-admonition-icon--{nom_boite}: url('');\n\n"

    if color is not None:
        code += f"--color-{nom_boite} : var(--color-{color});\n\n"


    code += f'.md-typeset .admonition.{nom_affiche},\n'
    code += f'.md-typeset details.{nom_affiche}' + ' {\n'
    code += f'\tborder-color: {col};\n' + '}\n'

    code += f'.md-typeset .{nom_affiche}> .admonition-title,\n.md-typeset .{nom_affiche}> summary'+ '{\n '
    code += f'\tbackground-color: rgba({col},0.1);\n\tborder-color: rgb({col});\n'+'}\n'

    code += f'.md-typeset .{nom_affiche}> .admonition-title::before,\n.md-typeset .{nom_affiche}> summary::before ' + '{\n'
    code += f'\tbackground-color: rgb({col});\n\t-webkit-mask-image: var({icn});\n          mask-image: var({icn});'+ '}\n\n'

    # code pour les onglets à l'intérieur de la boîte
    code += f'.md-typeset .{nom_affiche} .tabbed-set > input:checked + label '+  '{\n'
    code += f'\tcolor : rgb({col});\n\tborder-color : rgb({col});\n'+ '}\n'

    code += f'.md-typeset .{nom_affiche} .tabbed-set > input:not(.focus-visible) + label:hover' + '{\n'
    code += f'\tcolor : rgb({col});\n' + '}\n\n'

    code += f'/* fin {nom_affiche} */\n\n'

    return code




print(boite('définition','defn',None,None))

# print(boite('propriété','prop',None,None))
