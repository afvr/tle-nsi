from copy import deepcopy
import numpy as np
import drawSvg as draw

TAILLE_CASE = 40
TAILLE_POLICE = 20

def sudoku(grille, fichier):
    largeur, hauteur = TAILLE_CASE*(len(grille[0])), TAILLE_CASE*(len(grille))
    d = draw.Drawing(largeur + TAILLE_CASE, hauteur + TAILLE_CASE, origin=(-0.5*TAILLE_CASE,-0.5*TAILLE_CASE))

    r = draw.Rectangle(0, 0, largeur, hauteur, fill='white')
    d.append(r)

    n = 9

    for y in range(9):
        for x in range(9):
            r = draw.Rectangle(x*TAILLE_CASE,
                               y*TAILLE_CASE,
                               TAILLE_CASE, TAILLE_CASE, fill="none",
                               stroke_width=1,
                               stroke='black')
            d.append(r)
            if grille[n-1-y][x] > 0 :
                d.append(draw.Text(str(grille[n-1-y][x]),
                           TAILLE_POLICE,
                           x*TAILLE_CASE + 0.5*TAILLE_CASE,
                           y*TAILLE_CASE + 0.5*TAILLE_CASE,
                           text_anchor='middle',
                           valign='middle'))
            else :
                continue

    for y in range(3):
        for x in range(3):
            r = draw.Rectangle(x*TAILLE_CASE*3,
                               y*TAILLE_CASE*3,
                               TAILLE_CASE*3, TAILLE_CASE*3, fill="none",
                               stroke_width=3,
                               stroke='black')
            d.append(r)

    d.saveSvg(f'{fichier}.svg')
    d.savePng(f'{fichier}.png')


def sudoku_modelisation(fichier):
    largeur, hauteur = TAILLE_CASE*(len(grille[0])), TAILLE_CASE*(len(grille))
    d = draw.Drawing(largeur + TAILLE_CASE, hauteur + TAILLE_CASE, origin=(-0.5*TAILLE_CASE,-0.5*TAILLE_CASE))

    r = draw.Rectangle(0, 0, largeur, hauteur, fill='white')
    d.append(r)
    for y in range(9):
        for x in range(9):
            r = draw.Rectangle(x*TAILLE_CASE,
                               y*TAILLE_CASE,
                               TAILLE_CASE, TAILLE_CASE, fill="none",
                               stroke_width=1,
                               stroke='gray')
            d.append(r)

    z = 0
    for y in range(2,-1,-1):
        for x in range(3):
            r = draw.Rectangle(x*TAILLE_CASE*3,
                               y*TAILLE_CASE*3,
                               TAILLE_CASE*3, TAILLE_CASE*3, fill="none",
                               stroke_width=3,
                               stroke='black')
            d.append(r)

            d.append(draw.Text(str(z),
                           TAILLE_POLICE*3,
                           x*TAILLE_CASE*3 + 1.5*TAILLE_CASE,
                           y*TAILLE_CASE*3 + 1.5*TAILLE_CASE,
                           text_anchor='middle',
                           valign='middle'))
            z = z + 1

    d.saveSvg(f'{fichier}.svg')
    d.savePng(f'{fichier}.png')

grille = [  [0,0,5, 0,0,3, 8,0,0],\
            [2,0,0, 0,0,0, 0,6,0],\
            [0,1,0, 0,0,0, 0,0,2],\

            [0,0,0, 4,0,0, 0,0,0],\
            [8,0,0, 0,0,0, 1,9,0],\
            [0,0,0, 9,0,0, 0,0,8],\

            [0,0,4, 0,0,8, 0,0,3],\
            [0,0,0, 7,0,6, 0,5,0],\
            [7,3,0, 1,0,4, 0,0,9]
        ]



#sudoku(grille, "sudoku")
sudoku_modelisation("grille")
