/* debut */
import geometry;
import mesmacros;
//import operation_relatifs;
//import tableau_proport;
//import instruments_geometrie;
//import trembling;
unitsize(1cm);
defaultpen(fontsize(12));
//defaultpen(linewidth(1));
//settings.tex="pdflatex";
settings.outformat="pdf";
//dotfactor=3;
//tremble tr=tremble();
//tremble tr=tremble(angle=10, frequency=0.5, random=50);


real l=0.7;
real r=0.1;

void drawgrillesudoku(pair pos, int[] valeurs, real l=l, int size=12, bool coord=true, pen p=currentpen, pen traits=currentpen)
{
  for (int i=0;i<10;++i)
  {
     pen p=traits;
     if (i%3==0)
          p=traits+linewidth(1.5);
     draw(pos+(0,-i*l)--pos+(9*l,-i*l),p);
     draw(pos+(i*l,0)--pos+(i*l,-9*l),p);
  }

  if (coord)
  {
     for(int i=0;i<9;++i)
     {
        label(tt(string(i)), pos+(0,-i*l-0.5*l),W,p+fontsize(size-2));
        label(tt(string(i)), pos+(i*l+0.5*l,0),N,p+fontsize(size-2));	
     }
  }

  for (int i=0;i<9;++i)
  {
     for (int j=0;j<9;++j)
     {
        if (valeurs[9*i+j]>0)
           label(string(valeurs[9*i+j]),pos+(j*l+.5*l,-i*l-0.5*l),p+fontsize(size));
     }
  }
}

void affichernombres(pair pos, int[] valeurs,real l=l, int size=12,pen p=currentpen)
{
  for (int i=0;i<9;++i)
  {
     for (int j=0;j<9;++j)
     {
        if (valeurs[9*i+j]>0)
           label(string(valeurs[9*i+j]),pos+(j*l+.5*l,-i*l-0.5*l),p+fontsize(size));
     }
  }
}

int[] grille_diff(pair[] modifs,int[] grille_vierge = new int[] {})
{
  while (grille_vierge.length<81)
        grille_vierge.push(0);

  for (pair p : modifs)
  {
    int x = (int)(p.x);
    int v = (int)(p.y);
    grille_vierge[x]=v;
  }
  return grille_vierge;
}



void barrer_case(int n, real l=l, pen p=red)
{
  int x=n%9;
  int y=quotient(n,9);
  real x1=l*x;
  real x2=l*(x+1);
  real y1=-l*y;
  real y2=-l*(y+1);
  draw((x1,y1)--(x2,y2),p);
  draw((x1,y2)--(x2,y1),p);
}


/* couche 1 */
int [] diabolique = {3, 7, 0, 0, 0, 9, 2, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 5, 5, 0, 0, 2, 7, 0, 0, 4, 0, 7, 0, 0, 6, 0, 0, 0, 0, 2, 0, 0, 0, 0, 9, 0, 0, 0, 0, 6, 0, 0, 0, 0, 3, 0, 0, 7, 0, 6, 0, 0, 2, 8, 0, 0, 4, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 9, 1, 0, 0, 0, 5, 6};

drawgrillesudoku((0,0), diabolique, coord=false);


