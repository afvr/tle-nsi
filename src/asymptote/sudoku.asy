/* debut */
import geometry;
import mesmacros;
//import operation_relatifs;
//import tableau_proport;
//import instruments_geometrie;
//import trembling;
unitsize(1cm);
defaultpen(fontsize(12));
//defaultpen(linewidth(1));
//settings.tex="pdflatex";
settings.outformat="pdf";
//dotfactor=3;
//tremble tr=tremble();
//tremble tr=tremble(angle=10, frequency=0.5, random=50);


real l=0.7;
real r=0.1;

void drawgrillesudoku(pair pos, int[] valeurs, real l=l, int size=12, bool coord=true, pen p=currentpen, pen traits=currentpen)
{
  for (int i=0;i<10;++i)
  {
     pen p=traits;
     if (i%3==0)
          p=traits+linewidth(1.5);
     draw(pos+(0,-i*l)--pos+(9*l,-i*l),p);
     draw(pos+(i*l,0)--pos+(i*l,-9*l),p);
  }

  if (coord)
  {
     for(int i=0;i<9;++i)
     {
        label(tt(string(i)), pos+(0,-i*l-0.5*l),W,p+fontsize(size-2));
        label(tt(string(i)), pos+(i*l+0.5*l,0),N,p+fontsize(size-2));	
     }
  }

  for (int i=0;i<9;++i)
  {
     for (int j=0;j<9;++j)
     {
        if (valeurs[9*i+j]>0)
           label(string(valeurs[9*i+j]),pos+(j*l+.5*l,-i*l-0.5*l),p+fontsize(size));
     }
  }
}

void affichernombres(pair pos, int[] valeurs,real l=l, int size=12,pen p=currentpen)
{
  for (int i=0;i<9;++i)
  {
     for (int j=0;j<9;++j)
     {
        if (valeurs[9*i+j]>0)
           label(string(valeurs[9*i+j]),pos+(j*l+.5*l,-i*l-0.5*l),p+fontsize(size));
     }
  }
}

int[] grille_diff(pair[] modifs,int[] grille_vierge = new int[] {})
{
  while (grille_vierge.length<81)
        grille_vierge.push(0);

  for (pair p : modifs)
  {
    int x = (int)(p.x);
    int v = (int)(p.y);
    grille_vierge[x]=v;
  }
  return grille_vierge;
}



void barrer_case(int n, real l=l, pen p=red)
{
  int x=n%9;
  int y=quotient(n,9);
  real x1=l*x;
  real x2=l*(x+1);
  real y1=-l*y;
  real y2=-l*(y+1);
  draw((x1,y1)--(x2,y2),p);
  draw((x1,y2)--(x2,y1),p);
}


/* couche 1 */
int [] diabolique = {3, 7, 0, 0, 0, 9, 2, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 5, 5, 0, 0, 2, 7, 0, 0, 4, 0, 7, 0, 0, 6, 0, 0, 0, 0, 2, 0, 0, 0, 0, 9, 0, 0, 0, 0, 6, 0, 0, 0, 0, 3, 0, 0, 7, 0, 6, 0, 0, 2, 8, 0, 0, 4, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 9, 1, 0, 0, 0, 5, 6};

drawgrillesudoku((0,0), diabolique, coord=false);


/* couche 2 */
int [] tres_facile = {9, 6, 2, 1, 0, 0, 3, 7, 8, 
                      1, 8, 5, 6, 7, 3, 4, 2, 9, 
		      0, 7, 4, 2, 0, 0, 5, 0, 1, 
		      4, 9, 6, 5, 1, 7, 8, 3, 2, 
		      2, 1, 8, 9, 3, 6, 7, 4, 5, 
		      7, 5, 3, 8, 0, 4, 1, 9, 6, 
		      5, 3, 1, 7, 6, 2, 9, 8, 4, 
		      8, 2, 7, 4, 5, 0, 0, 0, 0, 
		      6, 4, 0, 3, 0, 1, 0, 0, 7};

drawgrillesudoku((0,0), tres_facile);

/* couche 3 */

int[] valeurs = {
0,0,8,0,3,0,0,0,0,
0,1,0,0,0,0,7,0,2,
4,0,0,0,8,0,1,0,9,
0,0,5,8,0,4,0,0,0,
0,0,0,6,0,0,9,7,0,
0,0,7,1,0,5,0,0,0,
5,0,0,0,6,0,3,0,4,
0,8,0,0,0,0,5,0,7,
0,0,4,0,1,0,0,0,0
};

real l = .35;

real v = -4;

drawgrillesudoku((0,0), valeurs, l=l, size=10, coord=false);

draw((4.5*l,-9*l)--(4.5l,v),Arrow(TeXHead));

int[] grille_modif1 = grille_diff(new pair[] {(40, 2), (41, 3), (49, 9), (67, 4), (13, 5), (31, 7), (37, 4), (38, 1), (36, 8), (44, 5), (8, 6), (80, 8), (6, 4), (7, 5), (25, 3), (53, 3), (16, 8), (35, 1)});

drawgrillesudoku((0,v), valeurs, l=l, size=10, coord=false);
affichernombres((0,v), grille_modif1,l=l, size=10, red);


draw((1.5*l,-9*l+v)--((-7+4.5)*l,2v),Arrow(TeXHead));

draw((7.5*l,-9*l+v)--((7+4.5)*l,2v),Arrow(TeXHead));

drawgrillesudoku((-7*l,2v), valeurs, l=l, size=10, coord=false);
affichernombres((-7*l,2v), grille_modif1,l=l, size=10, red);
affichernombres((-7*l,2v),grille_diff(new pair[] {(12,9)}), l=l, size=10,blue);

drawgrillesudoku((7*l,2v), valeurs, l=l, size=10, coord=false);
affichernombres((7*l,2v), grille_modif1,l=l, size=10, red);
affichernombres((7*l,2v),grille_diff(new pair[] {(12,4)}), l=l, size=10,blue);

draw(((7+4.5)*l,2v-9*l)--((7+4.5)*l,3v),Arrow(TeXHead));
label("?", ((7+4.5)*l,3v), S);

draw(((-7+4.5)*l,2v-9*l)--((-7+4.5)*l,3v),Arrow(TeXHead));
label("?", ((-7+4.5)*l,3v), S);

/* couche 4 */
int[] valeurs = {
0,0,8,0,3,0,0,0,0,
0,1,0,0,0,0,7,0,2,
4,0,0,0,8,0,1,0,9,
0,0,5,8,0,4,0,0,0,
0,0,0,6,0,0,9,7,0,
0,0,7,1,0,5,0,0,0,
5,0,0,0,6,0,3,0,4,
0,8,0,0,0,0,5,0,7,
0,0,4,0,1,0,0,0,0
};


drawgrillesudoku((0,0), valeurs, l=l, coord=true);

int[] grille_modif1 = grille_diff(new pair[] {(40, 2), (41, 3), (49, 9), (67, 4), (13, 5), (31, 7), (37, 4), (38, 1), (36, 8), (44, 5), (8, 6), (80, 8), (6, 4), (7, 5), (25, 3), (53, 3), (16, 8), (35, 1)});

affichernombres((0,0), grille_modif1,l=l, red);

affichernombres((0, 0),grille_diff(new pair[] {(12,9)}), l=l,blue);

